<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @package owesome
 */
get_header(); ?>

<!--==================== main content section ====================-->
<main id="content">
  <?php get_template_part('navbar','');?>

<div class="clearfix"></div>

    <div class="main-layout">
      <div class="row">  
        <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
          <div class="page-content">
            <?php get_template_part('slider','');?>
            <div class="row">
              <div class="col-md-12">
                <div class="grid">
                  <?php 
                    while(have_posts()) { the_post();
                      get_template_part('content','');
                    ?>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 text-center paginationcontent">
                <?php
            			//Previous / next page navigation
            			the_posts_pagination( array(
            			'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
            			'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
            			'screen_reader_text' => ' ',
            			) );
      			    ?>
              </div>
            </div>
          </div>
        </div>
        <aside class="col-md-3 col-lg-3">
          <?php get_sidebar(); ?>
        </aside>
      </div>
    </div>
</main>
<?php
get_footer();
?>