<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package owesome
 */ ?>
<!--==================== ow-FOOTER AREA ====================-->
      <footer> 
        <div class="row">
          <div class="col-lg-12">
            <div class="overlay"> 
              <!--Start ow-footer-widget-area-->
              <?php if ( is_active_sidebar( 'footer_widget_area' ) ) { ?>
              <div class="ow-footer-widget-area">
                    <?php  dynamic_sidebar( 'footer_widget_area' ); ?>
              </div>
              <?php } ?>
              <!--End ow-footer-widget-area-->
              <div class="ow-footer-copyright">
                  <div class="row">
                    
                    <div class="text-center hidden-md hidden-lg col-xs-12">
            			    <ul class="ow-social">
                                   <?php if(get_theme_mod('social_link_facebook')) { ?>
                        <li><span class="icon-soci"> <a href="<?php echo esc_url(get_theme_mod('social_link_facebook')); ?>" <?php if(get_theme_mod('Social_link_facebook_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-facebook"></i></a></span></li>
                        <?php } if(get_theme_mod('social_link_twitter')) { ?>
                        <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_twitter')); ?>" <?php if(get_theme_mod('Social_link_twitter_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-twitter"></i></a></span></li>
                        <?php } if(get_theme_mod('social_link_linkedin')) { ?>
                        <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_linkedin')); ?>" <?php if(get_theme_mod('Social_link_linkedin_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-linkedin"></i></a></span></li>
                        <?php } if(get_theme_mod('social_link_google')) { ?>
                        <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_google')); ?>" <?php if(get_theme_mod('Social_link_google_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-google-plus"></i></a></span></li>
                        <?php } ?>
                      </ul>
                    </div>
                    
                  </div>
              </div>
            </div>
          </div>
      </div>

      <div class="text-center copyright">
        <p>Copyright &copy; <?php echo esc_html(date_i18n(__('Y','owesome'))).' '; bloginfo( 'name' ); ?> | <?php printf( esc_html__( 'Theme by %1$s', 'owesome' ),  '<a href="'.esc_url('https://wpcount.com/').'" rel="designer">WPCount</a>.' ); ?> | <?php printf( esc_html__( 'Powered by %1$s', 'owesome' ),  '<a href="'.esc_url('https://wordpress.org/').'">WordPress</a>.' ); ?></p>
      </div>

      </footer>
    </div>

<!--Scroll To Top--> 
<a href="#" class="ti_scroll bounceInRight  animated"><i class="fa fa-angle-double-up"></i></a> 
<!--/Scroll To Top-->
<?php wp_footer(); ?>
</body>
</html>