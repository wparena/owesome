<?php
/**
 * The template for displaying search results pages.
 *
 * @package owesome
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
		<?php get_template_part('navbar','');?>
    <div class="row">
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
			<div class="page-content">
	        <?php 
			global $i;
			if ( have_posts() ) : ?>
			<h2><?php printf( esc_html__( "Search Results for: %s", 'owesome' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
	        	<div class="grid">
					<?php while ( have_posts() ) : the_post();  
					 get_template_part('content','');
					 endwhile; ?>
				</div>
				<div class="col-lg-12 text-center">
		          	<?php
					//Previous / next page navigation
					the_posts_pagination( array(
					'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
					'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
					'screen_reader_text' => ' ',
					) );
					?>
	         	</div>
		      	<div class="clearfix"></div>
					<?php else : ?>
					<h2><?php esc_html_e('Not Found','owesome'); ?></h2>
					<p><?php esc_html_e('Sorry, Do Not match.','owesome' ); ?>
					</p>
					<?php get_search_form(); ?>
					<?php endif; ?>
			</div>
      </div>
	  <aside class="col-md-3 col-lg-3">
        <?php get_sidebar(); ?>
      </aside>
    </div>
</main>
<?php
get_footer();
?>