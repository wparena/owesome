<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package owesome
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="headerbkcolor">
        <header>
          <div class="clearfix"></div>
          <div class="ow-main-nav">
            <div class="row">
              <div class="col-md-7 col-lg-7 col-sm-7">
                <div class="navbar-header">
                <!-- Logo -->
                <?php
                if(has_custom_logo())
                  {
                  // Display the Custom Logo
                  echo '<div class="logo-div">'; 
                    the_custom_logo();
                    echo "<div class='clearfix'></div>";
                  echo '</div>';
                  }
                ?>
                <div>
                  <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>"><span class="site-title"><?php bloginfo('name'); ?></span>
                      <br>
                      <span class="site-description"><?php echo esc_html(get_bloginfo( 'description', 'display' )); ?></span>   
                  </a>
                </div>
                <!-- Logo -->
                </div>
              </div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <div class="text-right hidden-xs">
                  <ul class="ow-social">
                  <?php if(get_theme_mod('social_link_facebook')) { ?>
                    <p class="updates"><?php esc_html_e('Follow Us','owesome') ?></p>
                  <?php } ?>
                    <?php if(get_theme_mod('social_link_facebook')) { ?>
                    <li><span class="icon-soci"> <a href="<?php echo esc_url(get_theme_mod('social_link_facebook')); ?>" <?php if(get_theme_mod('Social_link_facebook_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-facebook"></i></a></span></li>
                    <?php } if(get_theme_mod('social_link_twitter')) { ?>
                    <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_twitter')); ?>" <?php if(get_theme_mod('Social_link_twitter_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-twitter"></i></a></span></li>
                    <?php } if(get_theme_mod('social_link_linkedin')) { ?>
                    <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_linkedin')); ?>" <?php if(get_theme_mod('Social_link_linkedin_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-linkedin"></i></a></span></li>
                    <?php } if(get_theme_mod('social_link_google')) { ?>
                    <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_google')); ?>" <?php if(get_theme_mod('Social_link_google_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-google-plus"></i></a></span></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    </div>
  </div>
<!-- #masthead -->

<?php if( get_header_image() != '') : ?>
  <div class="header-image">
    <img src="<?php echo esc_url( get_header_image() ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'title' ) ) ; ?>" />
  </div>
<?php endif; ?>
