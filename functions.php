<?php
/**
 * owesome functions and definitions.
 *
 *
 * @package owesome
 */


	$owesome_theme_path = get_template_directory() . '/inc/';

	require( $owesome_theme_path . '/custom-navwalker.php' );
	require( $owesome_theme_path . '/font/font.php');

	/*-----------------------------------------------------------------------------------*/
	/*	Enqueue scripts and styles.
	/*-----------------------------------------------------------------------------------*/
	require( $owesome_theme_path .'/enqueue.php');
	/* ----------------------------------------------------------------------------------- */
	/* Customizer */
	/* ----------------------------------------------------------------------------------- */

	require( $owesome_theme_path . '/customize/customize_copyright.php');
	require( $owesome_theme_path . '/customize/customize_control/class-customize-alpha-color-control.php');
	
	
	
	

if ( ! function_exists( 'owesome_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function owesome_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on owesome, use a find and replace
	 * to change 'owesome' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'owesome', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __('Primary menu','owesome' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	//Custom Logo
	add_theme_support( 'custom-logo');

	function owesome_the_custom_logo() {
		the_custom_logo();
	}

	add_filter('get_custom_logo','owesome_logo_class');


	function owesome_logo_class($html)
	{
	$html = str_replace('custom-logo-link', 'navbar-brand', $html);
	return $html;
	}

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'owesome_custom_background_args', array(
		'default-color' => 'eeeeee',
		'default-image' => '',
	) ) );

	/* Add Support For Custom Header */
	add_theme_support( 'custom-header' );

}
endif;
add_action( 'after_setup_theme', 'owesome_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function owesome_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'owesome_content_width', 640 );
}
add_action( 'after_setup_theme', 'owesome_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function owesome_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'owesome' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="ow-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area', 'owesome' ),
		'id'            => 'footer_widget_area',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="col-md-5 col-sm-5 rotateInDownLeft animated ow-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'owesome_widgets_init' );


function owesome_enqueue_customizer_controls_styles() {
  wp_register_style( 'owesome-customizer-controls', get_template_directory_uri() . '/css/customizer-controls.css', NULL, NULL, 'all' );
  wp_enqueue_style( 'owesome-customizer-controls' );
  }
add_action( 'customize_controls_print_styles', 'owesome_enqueue_customizer_controls_styles' );


/* Custom template tags for this theme. */
require get_template_directory() . '/inc/template-tags.php';

//Read more Button on slider & Post
function owesome_read_more() {
	
	global $post;
	
	$readbtnurl = '<br><a class="btn btn-tislider-two" href="' . esc_url(get_permalink()) . '">'.__('Read More','owesome').'</a>';
	
    return $readbtnurl;
}
add_filter( 'the_content_more_link', 'owesome_read_more' );

// Changing excerpt more
   	function owesome_excerpt_more($more) {
   	if ( is_admin() ){
		return $more;
	}
   	global $post;
   	return '';
   	}
   	add_filter('excerpt_more', 'owesome_excerpt_more');

//change text to leave a reply on comment form
	function owesome_comment_reform ($arg) {
	if ( is_admin() ){
		return $arg;
	}
	$arg['title_reply'] = __('Post a Comment:','owesome');
	return $arg;
	}
	add_filter('comment_form_defaults','owesome_comment_reform');
