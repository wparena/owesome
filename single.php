<!-- =========================
     Page Breadcrumb   
============================== -->
<?php get_header(); ?>
<div class="clearfix"></div>
<!-- =========================
     Page Content Section      
============================== -->
 <main id="content">

  <?php get_template_part('navbar','');?>
  
  <div class="row"> 
      <!--/ Main blog column end -->
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
        <div class="page-content">
  		      <?php if(have_posts())
  		        {
  		      while(have_posts()) { the_post(); ?>
              <div class="ow-blog-post-box">
                <article class="small">

                  <?php if(has_post_thumbnail()): ?>
                    <a>

                      <?php if(has_post_thumbnail()): ?>
                      <?php $defalt_arg =array('class' => "img-responsive"); ?>
                      <?php the_post_thumbnail('', $defalt_arg); ?>
                      <?php endif; ?>

                    </a>
                  <?php endif; ?>

                  <h1><?php the_title(); ?></h1>

                  <div class="ow-blog-category post-meta-data"> 
            
                    <i class="fa fa-user"></i><?php the_author_posts_link(); ?>
                    <i class="fa fa-calendar"></i><span><?php echo esc_html(get_the_date( get_option( 'date_format' ))); ?></span>
                    
                  </div>

                  <?php the_content(); ?>
                  <?php wp_link_pages( array( 'before' => '<div class="link single-page-break">' . __( 'Pages:', 'owesome' ), 'after' => '</div>' ) ); ?>
                </article>
              </div>
  		      <?php } ?>
  		      <?php } ?>
           <?php comments_template('',true); ?>
        </div>
      </div>
      <aside class="col-md-3 col-lg-3">
      <?php get_sidebar(); ?>
      </aside>
    <!--/ Row end --> 
  </div>
</main>
<?php get_footer(); ?>