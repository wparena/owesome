=== Owesome ===

Contributors: wpcount
Requires at least: WordPress 4.7
Tested up to: WordPress 5.0-trunk
Version: 2.0.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-cloumn, two-columns, right-sidebar, custom-logo, sticky-post, flexible-header, custom-menu, featured-images, custom-background, footer-widgets, blog, threaded-comments, translation-ready

== Description ==

Owesome is CSS3 Powered and WordPress Ready Responsive Theme. Super Elegant and Professional Owesome Theme will be adjusted automatically with all smart devices whether it is Mobile screen or  Laptop screen. This theme uses the grid layout system to show posts. Owesome Theme features include a homepage template with posts shown in grid layout, two widget areas, one menu location and social-nav.
Owesome is Created with Twitter Bootstrap 3.3.7 Framework. Owesome is a great design idea for websites like Personal Portfolio, and Blogs. Create Outstanding Website or Blog in Minutes!.

Theme has two layouts.A "single column layout" and a "two columns layout". To make your website in two columns use sidebar widget section. In two columms layout the content is on left side and a right sidebar widget. 
We focused on usability across various devices, starting with smart phones.it is compatible with various devices. Owesome is a Cross-Browser Compatible theme that works on All leading web browsers. Owesome is easy to use and 
user friendly theme.

To make your site attractive it has two widget sections first for right-sidebar widget section and 
second for Footer widget sections.

To set custom menu in header set primary location. We also added social media links to add your social links.It boasts of beautifully designed page sections , Home, Blog and Default Page Template(page with right sidebar). Owesome is translation ready theme with WPML compatible & Many More.

This theme is compatible with Wordpress Version 4.7  and above and it supports the new theme customization API (https://codex.wordpress.org/Theme_Customization_API).

Supported browsers: Firefox, Opera, Chrome, Safari and IE11 (Some css3 styles like shadows, rounder corners and 2D transform are not supported by IE8 and below).

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.

2. Type in Owesome in the search form and press the 'Enter' key on your keyboard.

3. Click on the 'Activate' button to use your new theme right away.

4. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

Owesome WordPress Theme, Copyright 2018 Jazib Zaman
Owesome is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Owesome bundles the following third-party resources:

===========================================
This theme uses Bootstrap as a design tool
===========================================
* Bootstrap (http://getbootstrap.com/)
* Copyright (c) 2011-2016 Twitter, Inc
* Licensed under https://github.com/twbs/bootstrap/blob/master/LICENSE

============================
This theme uses Font Awesome 
============================
* Font Awesome
* Copyright: Dave Gandy
* Resource URI: http://fontawesome.io
* License: MIT
* License URI: https://opensource.org/licenses/mit-license.html

================================
This theme uses daneden/animate
================================
* Animate  : https://github.com/daneden
* Copyright (c) 2018 Daniel Eden ( https://github.com/daneden/animate.css/blob/master/LICENSE )
* Licensed under https://github.com/daneden/animate.css

=================================
This theme uses SmartMenus jQuery
=================================
* Version v1.0.0 - January 27, 2016
* http://www.smartmenus.org/
* Copyright Vasil Dinkov, Vadikom Web Ltd. http://vadikom.com; Licensed MIT

=====================================
All Images In ScreenShot Use Pixabay
=====================================
* By: Picography
* https://pixabay.com/en/notebook-laptop-work-pc-computer-405755/
* By: Rawpixel
* https://pixabay.com/en/paper-business-aerial-view-benefit-3190198/
* By: annca
* https://pixabay.com/en/easter-easter-bunny-hare-spring-3204589/
* License: CC0 Creative Commons

== Changelog ==

= 2.0.7 =

* Released: June 4, 2018
* Removal Of Bugs/Errors

= 2.0.6 =

* Released: May 23, 2018
* Archive Page Title Fix

= 2.0.5 =

* Released: May 9, 2018
* Removal Of Bugs/Errors

= 2.0.4 =

* Released: May 8, 2018
* Removal Of Bugs/Errors
* Some Rules Edition In Css

= 2.0.3 =

* Released: May 7, 2018
* Addition Of More Licenses
* Removal Of Bugs/Errors
* Minor changes In Templates

= 2.0.2 =

* Released: March 14, 2018
* Removal Of Bugs/Errors

= 2.0.1 =

* Released: Feb 24, 2018
* Redesign Structure Of Theme

= 2.0.0 =

* Released: Feb 16, 2018
* Addition Of CSS Rules
* Basic Changes In Theme's Layout

= 1.0.0 =

* Released: May 27, 2010

Initial release