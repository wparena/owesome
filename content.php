<?php 
/**
 * The template for displaying the content.
 * @package owesome
 */
?>
<div class="marginbottom">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="ow-blog-post-box col-md-12">
			<?php if(has_post_thumbnail()){ ?>
				<a href="<?php the_permalink(); ?>" class="gridimage col-md-12 col-xs-12">

					<?php if(has_post_thumbnail()): ?>
					<?php $defalt_arg =array('class' => "img-responsive"); ?>
					<?php the_post_thumbnail('', $defalt_arg); ?>
					<?php endif; ?>

				</a>
			<?php } else { ?>
				<a href="<?php the_permalink(); ?>" class="gridimage col-md-12 col-xs-12">
						<img src="<?php echo esc_url(get_template_directory_uri().'/images/perfect.jpg') ;?>">
				</a>
			<?php } ?>

			<article class="small">
				<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h1>
				<div class="ow-blog-category post-meta-data"> 
					
					<i class="fa fa-user"></i><?php the_author_posts_link(); ?>
					<br>
					<i class="fa fa-calendar"></i><span><?php echo esc_html(get_the_date( get_option( 'date_format' ))); ?></span>
					
				</div>
				<div class="categorycontent">
					<i class="fa fa-folder"></i><a href="#">
					<?php   $cat_list = get_the_category_list();
					if(!empty($cat_list)) { ?>
					<?php the_category(', '); ?>
					</a>
					<?php } ?>
				</div>

					<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'owesome' ), 'after' => '</div>' ) ); ?>
			</article>
		</div>
	</div>
</div>